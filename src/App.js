import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./Header";
import Home from "./Home";
import Login from "./Login";
import Checkout from "./Checkout";
import { useStateValue } from "./StateProvider";
import { useEffect } from "react";
import { auth } from "./firebase";
import Payment from "./Payment";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import Orders from "./Orders";

const stripePromise = loadStripe(
  "pk_test_51IHrMiESrutoWsbTT66U4ZRKRpGe26XFKiU2DJv1h4aKSCiutcpq9od2TiRt8xSmNoNEPIICD97Bco3XrivKuJPi000APcRI33"
);

const App = () => {
  const [_, dispatch] = useStateValue();
  useEffect(() => {
    const initBasket = (authUser) => {
      const dataBasket = localStorage.getItem("basket");
      if (dataBasket) {
        let basketDataObject = JSON.parse(localStorage.getItem("basket"));
        basketDataObject = basketDataObject[authUser.uid]?.basket;
        if (!!basketDataObject?.length) {
          dispatch({
            type: "INIT_BASKET",
            items: [...basketDataObject],
          });
        }
      }
    };

    auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        initBasket(authUser);
        dispatch({
          type: "SET_USER",
          user: authUser,
        });
      } else {
        dispatch({
          type: "SET_USER",
          user: null,
        });
        dispatch({
          type: "INIT_BASKET",
          items: [],
        });
      }
    });
  }, []);

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/checkout">
            <Header />
            <Checkout />
          </Route>
          <Route path="/payment">
            <Header />
            <Elements stripe={stripePromise}>
              <Payment />
            </Elements>
          </Route>
          <Route path="/orders">
            <Header />
            <Orders />
          </Route>
          <Route path="/">
            <Header />
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
