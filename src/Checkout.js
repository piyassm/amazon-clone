import React from "react";
import "./Checkout.css";
import CheckoutProduct from "./CheckoutProduct";
import { useStateValue } from "./StateProvider";
import Subtotal from "./Subtotal";

const Checkout = () => {
  const [{ basket, user }] = useStateValue();
  console.log("user", user);
  return (
    <div className="checkout">
      <div className="checkout__left">
        <img
          src="https://cdn.pixabay.com/photo/2015/11/28/11/26/sale-1067126_1280.jpg"
          alt=""
          className="checkout__ad"
        />
        {basket.length === 0 ? (
          <div>
            <h2>Your Shopping Basket is empty</h2>
            <p>
              You have no items in your basket. To buy one or more items, click
              "Add to basket" next to the item.
            </p>
          </div>
        ) : (
          <div>
            <h2>Hello, {user?.email}</h2>
            <h2>Your Shopping Basket</h2>
            {basket.map((item, key) => (
              <CheckoutProduct key={key} {...item} />
            ))}
          </div>
        )}
      </div>
      <div className="checkout__right">
        {basket.length > 0 && (
          <div>
            <Subtotal />
          </div>
        )}
      </div>
    </div>
  );
};

export default Checkout;
