import React, { useState, useEffect } from "react";
import "./Payment.css";
import CheckoutProduct from "./CheckoutProduct";
import { useStateValue } from "./StateProvider";
import { Link, useHistory } from "react-router-dom";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { getBasketTotal } from "./reducer";
import axios from "./axios";
import { db } from "./firebase";

const Payment = () => {
  const [{ basket, user }, dispatch] = useStateValue();

  const history = useHistory();

  const stripe = useStripe();
  const elements = useElements();

  const [succeeded, setSucceeded] = useState(false);
  const [processing, setProcessing] = useState("");
  const [error, setError] = useState("");
  const [disabled, setDisabled] = useState(false);
  const [clientSecret, setClientSecret] = useState(false);

  useEffect(() => {
    const getClientSecret = async () => {
      setClientSecret(false);
      const response = await axios({
        method: "get",
        url: `/payment/create?total=${getBasketTotal(basket) * 100}`,
      });
      setClientSecret(response.data.clientSecret);
    };
    getClientSecret();
  }, [basket]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    setProcessing(true);
    const cardElement = elements.getElement(CardElement);
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: cardElement,
    });

    if (error) {
      console.log("[error]", error);
      setProcessing(false);
    } else {
      console.log("[PaymentMethod]", paymentMethod);
      if (clientSecret) {
        const payload = await stripe
          .confirmCardPayment(clientSecret, {
            payment_method: {
              card: cardElement,
            },
          })
          .then(({ paymentIntent }) => {
            db.collection("users")
              .doc(user?.uid)
              .collection("orders")
              .doc(paymentIntent.id)
              .set({
                basket,
                amount: paymentIntent.amount,
                created: paymentIntent.created,
              });
            setSucceeded(true);
            setError("");
            setProcessing(false);
            dispatch({
              type: "EMPTY_BASKET",
            });
            history.replace("/orders");
          })
          .catch((error) => {
            setProcessing(false);
            console.log("error", error.message);
          });
      }
    }
  };

  const handleChange = (e) => {
    setDisabled(e.empty);
    setError(e.error ? e.error.message : "");
  };

  return (
    <div className="payment">
      <div className="paymeny__container">
        <h1>
          Checkout (<Link to="/checkout">{basket.length} items</Link>)
        </h1>
        <div className="payment__section">
          <div className="payment__title">
            <h3>Delivery Address</h3>
          </div>
          <div className="payment__address">
            <p>{user?.email}</p>
            <p>123 Muang Siam</p>
            <p>Bangkok, Thailand</p>
          </div>
        </div>
        <div className="payment__section">
          <div className="payment__title">
            <h3>Review items and delivery</h3>
          </div>
          <div className="payment__items">
            {basket.map((item, key) => (
              <CheckoutProduct key={key} {...item} />
            ))}
          </div>
        </div>
        <div className="payment__section">
          <div className="payment__title">
            <h3>Payment Method</h3>
            <small>for test</small>
            <br />
            <small>card: 4242 4242 4242 4242</small>
            <br />
            <small>MM/YY: 04 / 24</small>
            <br />
            <small>CVC: 242</small>
            <br />
            <small>ZIP: 42424</small>
          </div>
          <div className="payment__detail">
            <form onSubmit={handleSubmit}>
              <CardElement onChange={handleChange} />

              <div className="payment__priceContainer">
                <h3>Order Total: ${getBasketTotal(basket)}</h3>
              </div>
              <button
                type="submit"
                disabled={succeeded || processing || disabled}
              >
                {processing ? "Processing" : "Buy Now"}
              </button>
              {error && <div>{error}</div>}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Payment;
