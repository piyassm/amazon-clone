import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import "./Login.css";
import { auth } from "./firebase";
import { useEffect } from "react";

const Login = () => {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [msgLogin, setMsgLogin] = useState("");
  const [msgRegister, setMsgRegister] = useState("");

  useEffect(() => {
    if (msgLogin) {
      setTimeout(() => {
        setMsgLogin("");
      }, 2000);
    }
  }, [msgLogin]);

  useEffect(() => {
    if (msgRegister) {
      setTimeout(() => {
        setMsgRegister("");
      }, 2000);
    }
  }, [msgRegister]);

  useEffect(() => {
    return () => {
      setEmail("");
      setPassword("");
      setMsgLogin("");
      setMsgRegister("");
    };
  }, []);

  const signIn = (e) => {
    e.preventDefault();

    auth
      .signInWithEmailAndPassword(email, password)
      .then((auth) => {
        if (auth) {
          history.push("/");
        }
      })
      .catch((error) => {
        setMsgLogin(error.message);
        console.log("error", error.message);
      });
  };

  const register = (e) => {
    e.preventDefault();
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((auth) => {
        if (auth) {
          history.push("/");
        }
      })
      .catch((error) => {
        setMsgRegister(error.message);
        console.log("error", error.message);
      });
  };

  return (
    <div className="login">
      <Link to="/">
        <img
          className="login__logo"
          src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Amazon.com-Logo.svg/240px-Amazon.com-Logo.svg.png"
          alt=""
        />
      </Link>
      <div className="login__container">
        <h1 className="login__title">Sign In</h1>
        <form onSubmit={signIn}>
          <label>E-mail</label>
          <input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder={'can use: test@test.com'}
          />

          <label>Password</label>
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder={'for test@test.com: 123456'}
          />

          {msgLogin && (
            <div className="login__msgLogin">{msgLogin}</div>
          )}
          <button type="submit" className="login__signInButton">
            Sign In
          </button>
          <p className="login__textPrivacy">
            By signing-in you agree to the amazone fake clone
          </p>
          {msgRegister && (
            <div className="login__msgRegister">{msgRegister}</div>
          )}
          <button onClick={register} className="login__registerButton">
            Create your Amazon Account
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
