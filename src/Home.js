import React from "react";
import "./Home.css";
import Product from "./Product";

const Home = () => {
  return (
    <div className="home">
      <img
        className="home__image"
        src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2020/PrimeDay/Fuji_TallHero_NonPrime_v2_en_US_2x._CB403670067_.jpg"
        alt=""
      />
      <div className="home__row">
        <Product
          id={1}
          title={"Learning1"}
          price={11.99}
          rating={4}
          image={
            "https://image.freepik.com/free-psd/folded-magazine-mockup-right-side-inside-front-cover_15431-308.jpg"
          }
        />
        <Product
          id={2}
          title={"Learning2"}
          price={14.99}
          rating={5}
          image={
            "https://image.freepik.com/free-vector/education-online-books-flyer-template_82472-470.jpg"
          }
        />
      </div>
      <div className="home__row">
        <Product
          id={3}
          title={"Learning3"}
          price={51.99}
          rating={4}
          image={"https://s3.images-iherb.com/nwy/nwy12114/v/26.jpg"}
        />
        <Product
          id={4}
          title={"Learning4"}
          price={249.99}
          rating={5}
          image={
            "https://images-na.ssl-images-amazon.com/images/I/71Swqqe7XAL._AC_SX466_.jpg"
          }
        />
        <Product
          id={5}
          title={"Learning5"}
          price={84.99}
          rating={5}
          image={"https://s3.images-iherb.com/nwy/nwy10741/v/11.jpg"}
        />
      </div>
      <div className="home__row">
        <Product
          id={6}
          title={"Learning6"}
          price={359.99}
          rating={4}
          image={
            "https://media.very.co.uk/i/very/P6LTG_SQ1_0000000071_CHARCOAL_SLf?%24300x400_retinamobilex2%24"
          }
        />
      </div>
    </div>
  );
};

export default Home;
