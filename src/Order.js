import React from "react";
import CheckoutProduct from "./CheckoutProduct";
import "./Order.css";
import { getBasketTotal } from "./reducer";

const Order = (props) => {
  const {
    id,
    data: { amount, basket, created },
  } = props;

  function getDate(timestamp) {
    const d = new Date(timestamp * 1000);
    const year = d.getFullYear();
    const month = d.getMonth() + 1;
    const date = d.getDate();
    return `${month}/${date}/${year}`;
  }

  return (
    <div className="order">
      <h2>Order</h2>
      <p>Order Created: {getDate(created)}</p>
      <p className="order__id">Order No: {id}</p>
      {basket.map((item, key) => {
        return <CheckoutProduct key={key} {...item} hideButton />;
      })}
      <h3>Order Total: ${getBasketTotal(basket)}</h3>
    </div>
  );
};

export default Order;
