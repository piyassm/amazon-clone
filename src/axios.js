import axios from "axios";

const instance = axios.create({
  // baseURL: "http://localhost:5001/clone-378b3/us-central1/api", //use this if you run api localhost
  baseURL: "https://us-central1-clone-378b3.cloudfunctions.net/api",
});

export default instance;
