import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAS5uGBiYVZJMIJqrzejXgnjWnls79yydo",
  authDomain: "clone-378b3.firebaseapp.com",
  databaseURL: "https://clone-378b3.firebaseio.com",
  projectId: "clone-378b3",
  storageBucket: "clone-378b3.appspot.com",
  messagingSenderId: "788508012553",
  appId: "1:788508012553:web:a8524bcec4076bd87008d8",
  measurementId: "G-2XLR4QN656",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };
