export const initialState = {
  basket: [],
  user: null,
};

export const getBasketTotal = (basket) => {
  return basket.reduce((amount, item) => item.price + amount, 0).toFixed(2);
};

function reducer(state, action) {
  let newState = state;
  switch (action.type) {
    case "ADD_TO_BASKET":
      if (!state.user?.uid) return newState;
      newState = {
        ...state,
        basket: [...state.basket, action.item],
      };
      localStorage.setItem(
        "basket",
        JSON.stringify({
          [state.user.uid]: { basket: [...state.basket, action.item] },
        })
      );
      return newState;
    case "REMOVE_FROM_BASKET":
      if (!state.user?.uid) return newState;
      let newBasket = [...state.basket];
      const index = state.basket.findIndex((item) => item.id === action.id);
      if (index >= 0) {
        newBasket.splice(index, 1);
      } else {
        console.warn(`Can't remove product (id: ${action.id})`);
      }
      newState = { ...state, basket: [...newBasket] };
      localStorage.setItem(
        "basket",
        JSON.stringify({
          [state.user.uid]: { basket: newBasket },
        })
      );
      return newState;
    case "INIT_BASKET":
      newState = {
        ...state,
        basket: [...action.items],
      };
      return newState;
    case "EMPTY_BASKET":
      newState = {
        ...state,
        basket: [],
      };
      localStorage.removeItem("basket");
      return newState;
    case "SET_USER":
      newState = {
        ...state,
        user: action.user,
      };
      return newState;
    default:
      return state;
  }
}

export default reducer;
