FROM node:14-alpine

WORKDIR /app_api
COPY package*.json ./

RUN npm install yarn && yarn install
RUN npm install -g firebase-tools
COPY . .
RUN cd functions

CMD ["firebase", "emulators:start"]