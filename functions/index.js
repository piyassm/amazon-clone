const functions = require("firebase-functions");
const express = require("express");
const cors = require("cors");
const stripe = require("stripe")(
  "sk_test_51IHrMiESrutoWsbTDyFym4q8X47bZkQeWdlmQtrwhefY72DNYH7oCTGvC87tpC2uHsX4uyTXmqKBGh4Mq6tQRIwr00As8rpxs8"
);

const app = express();

app.use(cors({ origin: true }));
app.use(express.json());

app.get("/", (req, res) => res.status(200).send("Hello"));

function amountCheck(req, res, next) {
  const total = req.query.total;
  if (total > 0) {
    return next();
  } else {
    return res.status(400).send({
      error: "This value must be greater than or equal to 1",
    });
  }
}
app.get("/payment/create", amountCheck, async (req, res) => {
  const total = req.query.total;
  console.log("for this>>>>", total);
  const paymentIntent = await stripe.paymentIntents.create({
    amount: total,
    currency: "usd",
  });
  res.status(201).send({
    clientSecret: paymentIntent.client_secret,
  });
});

exports.api = functions.https.onRequest(app);
